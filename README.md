# Metamorph Platform Repository #

A curated collection of platform models. This projects act as templates to link up to various platforms.

Each of the following locations includes the most up-to-date .adm for a platform:

Electric Imp\Electric Imp Module
Photon\Photon Shield Template
Arduino\Arduino_DUE_Shield_Advanced
Arduino\Arduino_DUE_Shield_Basic
Bluetooth\Bluetooth-System-Template