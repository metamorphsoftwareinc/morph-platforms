# Metamorph Bluetooth System #
## Daughter Card Template ##

This model captures the "boilerplate" needed to create a daughter card for this system. The template design includes the physical connector, a PCB template with power and ground planes, and connectorization for hooking up user content.